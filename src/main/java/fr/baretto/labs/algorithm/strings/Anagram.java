package fr.baretto.labs.algorithm.strings;

import java.util.List;
import java.util.Map;

import static java.util.Arrays.sort;
import static java.util.stream.Collectors.groupingBy;

public class Anagram {

    /**
     * Group all anagram together.
     * An anagram is a word or phrase formed by rearranging the letters of a different word or phrase.
     * The idea is to group all words which have the same signature.
     * The signature is the natural ordering of characters.
     * @param words
     * @return
     */
    public static Map<String,List<String>> groupBy(final List<String> words) {
      return words.stream()
              .collect(groupingBy(word -> sorted(word)));
    }

    private static String sorted(final String word) {
        char[] chars = word.toCharArray();
        sort(chars);
        return new String(chars);
    }
}

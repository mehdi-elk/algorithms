package fr.baretto.labs.algorithm.strings;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AnagramTest {

    @Test
    public void given_a_two_anagrams_should_returns_one_list(){
        List<String> words = new ArrayList<String>();
        words.add("abba");
        words.add("baab");

        Map<String, List<String>> anagrams = Anagram.groupBy(words);

        assertThat(anagrams.keySet().size()).isEqualTo(1);
    }

    @Test
    public void given_a_two_words_not_anagrams_should_returns_two_list(){
        List<String> words = new ArrayList<String>();
        words.add("abba");
        words.add("test");

        Map<String, List<String>> anagrams = Anagram.groupBy(words);

        assertThat(anagrams.keySet().size()).isEqualTo(2);
    }


    @Test
    public void given_a_two_words_anagrams_should_returns_two_list(){
        List<String> words = new ArrayList<String>();
        words.add("abba");
        words.add("baab");
        words.add("test");

        Map<String, List<String>> anagrams = Anagram.groupBy(words);

        assertThat(anagrams.keySet().size()).isEqualTo(2);

        assertThat(anagrams.get("aabb")).contains("abba","baab");
    }
}
